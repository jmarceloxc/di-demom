package com.dh.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("en")
public class GreetingServiceImpl implements GreetingService {

    public static final String GREETING = "Hello GreetingServiceImpl";

    @Override
    public String sayGreeting() {
        return GREETING;
    }
}
